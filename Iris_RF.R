setwd("C:/Users/Stef/Documents/Programming/R/Iris")

library(reshape)
library(reshape2)
library(xlsx)
library(plyr)
library(dplyr)
library(caret)
library(party)
library(randomForest)
library(class)

iris <- read.csv("Iris.csv", sep=",")
head(iris)
dim(iris)
summary(iris)


##Define Training / Testing Set
filas.entrenamiento <- sample(1:nrow(iris), 0.8 * nrow(iris))

iris.train <- iris[filas.entrenamiento,]
iris.test  <- iris[-filas.entrenamiento,]

##Create Model

iris.train$Test.Training <- iris.train$Species <- NULL

modelo.ctree <- ctree(iris$Species ~ ., data = iris.train)

reales <- iris.test$Species
predichos <- predict(modelo.ctree, iris.test)

table(reales, predichos)
error.rate <- mean(reales != predichos)
error.rate
